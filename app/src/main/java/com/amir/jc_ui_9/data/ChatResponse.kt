package com.amir.jc_ui_9.data

import com.amir.jc_ui_9.models.Chat

class ChatResponse {

    companion object{
        private var list = mutableListOf<Chat>()

        public fun getList(): List<Chat> {
            if (list.isEmpty())
                fillList()
                return list

        }

        private fun fillList() {
          list.add(Chat("Ema","Hi Anna"))
          list.add(Chat("Anna","Hi My Heart"))
          list.add(Chat("Ema","where are you?"))
          list.add(Chat("Anna","I'm at work"))
          list.add(Chat("Ema","Could we meet after your work? Let's go to the new restaurant"))
          list.add(Chat("Anna","OMG, I wanted actually to ask you, if you are in?"))
          list.add(Chat("Ema","HAHAHA, I read your mind"))
          list.add(Chat("Anna","don't scare me, haha. Im Happy"))
          list.add(Chat("Anna","see you!"))
        }
    }
}