package com.amir.jc_ui_9.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

//my colors
val AppBlack = Color(0xFF333333)
val AppGray = Color(0xFF808080)
val AppBlue = Color(0xFF0DB6FF)
val AppLight = Color(0xFFF9FDFF)
val AppLightGray = Color(0xFFF1F5F8)
val AppTextGray = Color(0xFFA7B1B4)
