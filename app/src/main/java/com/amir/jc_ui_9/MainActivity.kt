package com.amir.jc_ui_9

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material3.ExperimentalMaterial3Api

import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme

import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.amir.jc_ui_9.data.ChatResponse
import com.amir.jc_ui_9.data.ThisApp
import com.amir.jc_ui_9.models.Chat
import com.amir.jc_ui_9.ui.theme.AppBlack
import com.amir.jc_ui_9.ui.theme.AppBlue
import com.amir.jc_ui_9.ui.theme.AppGray
import com.amir.jc_ui_9.ui.theme.AppLight
import com.amir.jc_ui_9.ui.theme.AppLightGray
import com.amir.jc_ui_9.ui.theme.AppTextGray
import com.amir.jc_ui_9.ui.theme.JC_UI_9Theme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            JC_UI_9Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainView()
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainView() {
    Column(Modifier.fillMaxSize()) {
        //header
        TopHeader()
        //body
        LazyColumn(
            Modifier
                .fillMaxWidth()
                .weight(1f)
                .background(AppLight),
            // reverse layout, the list will be displayed reversed, The latest items will be on the bottom not on the top
            // reverseLayout = true
        ) {
            items(ChatResponse.getList().size) {
                val item = ChatResponse.getList()[it]
                val isFromMe = item.from == ThisApp.myName
                ChatBollon(isFromMe, item)
            }
        }
        //bottom
        ChatBox()
    }
}

@Composable
private fun ChatBox() {
    Row(
        Modifier
            .fillMaxWidth()
            .padding(15.dp)
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(50))
                .background(AppLightGray)
                .padding(5.dp)
        ) {
            var msg by remember { mutableStateOf("") }
            OutlinedTextField(
                value = msg,
                onValueChange = { msg = it },
                placeholder = { Text(text = "Message", color = AppTextGray) },
                singleLine = true,
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = Color.Transparent,
                    unfocusedBorderColor = Color.Transparent
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(end = 100.dp)
            )
            //attach
            IconButton(
                onClick = { /*TODO*/ },
                modifier = Modifier
                    .align(Alignment.CenterEnd)
                    .padding(end = 50.dp)
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_attachment_24),
                    contentDescription = "Attachment",
                    tint = AppTextGray
                )
            }
            //send
            IconButton(
                onClick = { /*TODO*/ },
                modifier = Modifier
                    .size(50.dp)
                    .clip(RoundedCornerShape(50.dp))
                    .background(Color.White)
                    .align(Alignment.CenterEnd)

            ) {
                Icon(
                    painter = painterResource(id = R.drawable.baseline_send_24),
                    contentDescription = "Send",
                    tint = AppBlue
                )
            }
        }
    }
}

@Composable
private fun ChatBollon(isFromMe: Boolean, item: Chat) {
    Box(modifier = Modifier.fillMaxWidth()) {
        Card(
            Modifier
                .width(200.dp)
                .padding(15.dp, 5.dp)
                .clip(
                    RoundedCornerShape(
                        15.dp,
                        15.dp,
                        if (isFromMe) 0.dp else 15.dp,
                        if (isFromMe) 15.dp else 0.dp,
                    )
                )
                .align(if (isFromMe) Alignment.CenterEnd else Alignment.CenterStart),
            backgroundColor = if (isFromMe) AppBlue else AppLightGray,
            elevation = 4.dp
        ) {
            Text(
                text = item.text, color =
                if (isFromMe) AppLightGray else AppBlack,
                modifier = Modifier.padding(15.dp)
            )
        }
    }
}

@Composable
private fun TopHeader() {
    Row(
        Modifier
            .fillMaxWidth()
            .padding(15.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        //img
        Image(
            painter = painterResource(id = R.drawable.girl),
            contentDescription = "User",
            Modifier
                .size(60.dp)
                .clip(RoundedCornerShape(50))
        )
        Spacer(modifier = Modifier.width(15.dp))
        //texts
        Column(Modifier.weight(1f)) {
            Text(text = "Anna", fontSize = 16.sp, color = AppBlack)
            Text(text = "Online", fontSize = 12.sp, color = AppGray)
        }
        //icon buttons
        IconButton(onClick = { /*TODO*/ }) {
            Icon(
                painter = painterResource(id = R.drawable.ic_call_24),
                contentDescription = "call",
                tint = AppBlue
            )
        }
        IconButton(onClick = { /*TODO*/ }) {
            Icon(
                painter = painterResource(id = R.drawable.ic_more_vert_24),
                contentDescription = "call",
                tint = AppGray
            )
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    JC_UI_9Theme {
        Greeting("Android")
    }
}