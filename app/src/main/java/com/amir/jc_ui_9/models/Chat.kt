package com.amir.jc_ui_9.models

data class Chat(var from: String, var text: String)
